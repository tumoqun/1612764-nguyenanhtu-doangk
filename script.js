
async function evtSubmit(e){
    loading();
    e.preventDefault();
    const URL = 'https://api.themoviedb.org/3';
    const strSearch = $('form input').val();
    const apiKey = '0bda53d87bc9095e38cbc7f2fbb2f504';

    if($('#option1').hasClass('active')){
    let endPoint = 'search/movie'; 

    const searchMovieReq = new Request(`${URL}/${endPoint}/?api_key=${apiKey}&query=${strSearch}`, {
        method: 'GET'
    });
    fetch(searchMovieReq)
        .then(res => res.json())
        .then(res => {
            listMovie = res.results;
            fillMovie(listMovie);
        });
    }
    if($('#option2').hasClass('active')){
        let endPoint='search/person';
        const searchPeopleReq = new Request(`${URL}/${endPoint}/?api_key=${apiKey}&query=${strSearch}`, {
            method: 'GET'
        });
        fetch(searchPeopleReq)
        .then(res=>res.json())
        .then(res=>{
            var listMovie=[];
            if(res.results===undefined || res.results.length==0){
                fillMovie(undefined);
                return;
            }
        for(const i of res.results){
            const getMovieCreditsReq = new Request(`${URL}/person/${i.id}/movie_credits?api_key=${apiKey}`, {
                method: 'GET'
            });
            fetch(getMovieCreditsReq)
            .then(res=>res.json())
            .then(res=>{
                for(const m of res.cast){
                    listMovie.push({
                        poster_path:m.poster_path,
                        overview:m.overview,
                        title:m.title,
                        id:m.id
                    });
                }
                fillMovie(listMovie);
            })
        }
        })
    }
}

function loadFirstMovies(){
    loading();
    const URL = 'https://api.themoviedb.org/3';
    let endPoint = 'movie/top_rated';
    const apiKey = '0bda53d87bc9095e38cbc7f2fbb2f504';
    let page=Math.floor(Math.random()*100);


    const searchMovieReq = new Request(`${URL}/${endPoint}/?api_key=${apiKey}&page=${page}`, {
        method: 'GET'
    });

    fetch(searchMovieReq)
        .then(res => res.json())
        .then(res => {
            listMovie = res.results;
            fillMovie(listMovie);
        });
    
}

function loading(){
    $('#list-movie').empty();
    $('#list-movie').append(`
    <div class="mt-3 " style="text-align: center;">
        <div >
          <div class="spinner-border text-success" style="margin:auto; width:10rem; height:10rem" role="status">
          </div>
          <p class="text-success"><font size="10">Searching movies...</font></p>
        </div>
      </div>    
    `)
}

function fillMovie(ms){
    $('#list-movie').empty();
    if(ms===undefined||ms.length==0){
        $('#list-movie').append(`
        <div class="col-10 bg-dark d-flex flex-wrap m-3 p-3 justify-content-center " >
            <font class="text-white" size="10">Movies not found</font>
        </div>
        `)
        return;
    }
    for(const m of ms){
        //thẻ phim
        //onclick loaddetail${m.imdbID}
        $('#list-movie').append(`
       <div class="col-3 pt-3 pb-3">
       <a href="#" class="movieID" data-id="${m.id}">
        <img src="https://image.tmdb.org/t/p/original${m.poster_path}" class="card-img-top" alt="${m.title}">
        <div class="card-body bg-white">
          <h5 class="card-title">${m.title}</h5></a>
          <p class="card-text">${m.overview}</p>
          <p class="card-text"><small class="text-muted"></small></p>
        </div>
      </div>
        `)
    }
}

function loadDetail(DetailMovieReq,directorName,DirectorReq,ReviewReq){
    window.scrollTo(0,0);
    $('#list-movie').empty();
    $('#list-movie').append(`
    <div class="col-7 pt-3 pb-3 p-1">
         <img src="https://image.tmdb.org/t/p/original${DetailMovieReq.backdrop_path}" class="card-img-top" alt="">
         </div>
                      <div class="col-5 pt-3 pb-3  ">
                            <div class="card-body bg-white">
                              <h5 class="card-title font-weight-bolder">${DetailMovieReq.title}</h5>
                              <p></p>
                              <span class="card-text font-weight-bold">Overview:</span>
                              ${DetailMovieReq.overview}
                              <p></p>
                              <span class="card-text font-weight-bold">Release Date:</span>
                              ${DetailMovieReq.release_date}
                              <p></p>
                              <span class="card-text font-weight-bold">Director:</span>
                              ${directorName}
                              <p></p>
                              <span class="card-text font-weight-bold">Genres:</span>
                              <p id="genres"></p>
                              <p class="card-text"><small class="text-muted"></small></p>
                            </div>
                          </div>
    `)
    for(var genres of DetailMovieReq.genres){
        $('#genres').append(`
            ${genres.name},
        `)
    }

    $('#list-movie').append(`
    <div class=" card-body p-1 m-1 bg-light d-flex align-items-center justify-content-center" id="Reviews-Button" >
        <div class="card-body text-center text-white" style="background-color:black" >
            <font size="10">Reviews</font>
        </div>
    </div>
        <div class="card-body bg-dark col-12 pt-3 pb-3 p-0 card-group d-flex flex-wrap justify-content-center" id="Reviews"></div>
    `)

    if(ReviewReq.results.length==0){
        $('#Reviews').append(`
        <div class="col-10 bg-secondary d-flex flex-wrap m-3 p-3 justify-content-center" >
            <font size="10">There is no review about this film</font>
        </div>
        `)
    }

    for(var reviews of ReviewReq.results)
    {
        $('#Reviews').append(`
    <div class="col-11 bg-secondary d-flex flex-wrap m-1 p-3" >
        <div class="col-1 mt-1">
            <img src="./images.jpg" class="card-img-top"  alt="">
        </div>
        <div class="col-11 ">
            <div class="card-body p-0 text-white">
                <h5 class="card-title font-weight-bolder">${reviews.author}</h5>
                    <p></p>
                    <span class="card-text">${reviews.content}</span>
            </div>
        </div>
    </div>
    `)
    }

    $('#list-movie').append(`
    <div class=" card-body p-1 m-1 bg-light d-flex align-items-center justify-content-center" >
        <div class="card-body text-center text-white" style="background-color:black" >
            <font size="10">Cast</font>
        </div>
    </div>
        <div class="card-body bg-dark col-12 pt-3 pb-3 p-0 card-group d-flex flex-wrap " id="cast"></div>
    `)
    for(var cast of DirectorReq.cast){
        $('#cast').append(`
        <div class="col-3 pt-3 pb-3 bg-dark">
        <a href="#" class="peopleID" data-id="${cast.id}">
        <img src="https://image.tmdb.org/t/p/original${cast.profile_path}" class="card-img-top" alt="${cast.name}">
        <div class="card-body bg-white">
          <h5 class="card-title">${cast.name}</h5>
          </a>
          <p class="card-text">as ${cast.character}</p>
        </div>
        
      </div>
        `)
        }
}

async function getMovieDetail(evt){
    evt.preventDefault();
    var id =  $(this).data("id");
    const URL = 'https://api.themoviedb.org/3';
    const apiKey = '0bda53d87bc9095e38cbc7f2fbb2f504';

    const searchReviewReq=await fetch(`${URL}/movie/${id}/reviews?api_key=${apiKey}`);
    ReviewReq=await searchReviewReq.json();


    const searchDetailMovieReq = await fetch(`${URL}/movie/${id}?api_key=${apiKey}`);
    DetailMovieReq=await searchDetailMovieReq.json();
    
    const searchDirectorReq= await fetch(`${URL}/movie/${id}/credits?api_key=${apiKey}`)
    const DirectorReq=await searchDirectorReq.json();


    let directorName = DirectorName(DirectorReq.crew);

    loadDetail(DetailMovieReq,directorName,DirectorReq,ReviewReq);
}

function DirectorName(A){
    for(i of A){
        if(i.job==="Director"){
            return i.name;
        }
    }
}

async function getPeopleDetail(evt){
    evt.preventDefault();
    var id=$(this).data("id");
    
    const URL = 'https://api.themoviedb.org/3';
    const apiKey = '0bda53d87bc9095e38cbc7f2fbb2f504';

    const searchDetailPeopleReq = await fetch(`${URL}/person/${id}?api_key=${apiKey}`);
    DetailPeopleReq=await searchDetailPeopleReq.json();

    const searchMovieCastReq= await fetch(`${URL}/person/${id}/movie_credits?api_key=${apiKey}`);
    MovieCastReq=await searchMovieCastReq.json();

    loadPeopleDetail(DetailPeopleReq,MovieCastReq);

}

function loadPeopleDetail(DetailPeopleReq,MovieCastReq){
    $('#list-movie').empty();
    $('#list-movie').append(`
    <div class="col-5 pt-3 pb-3 p-1">
         <img src="https://image.tmdb.org/t/p/original${DetailPeopleReq.profile_path}" class="card-img-top" alt="">
         </div>
                      <div class="col-7 pt-3 pb-3  ">
                            <div class="card-body bg-white">
                              <h5 class="card-title font-weight-bolder">${DetailPeopleReq.name}</h5>
                              <p></p>
                              <span class="card-text font-weight-bold">Birthday:</span>
                              ${DetailPeopleReq.birthday}
                              <p></p>
                              <span class="card-text font-weight-bold">Place of birth:</span>
                              ${DetailPeopleReq.place_of_birth}
                              <p></p>
                              <span class="card-text font-weight-bold">Biography:</span>
                              ${DetailPeopleReq.biography}
                              <p></p>
                            </div>
                          </div>
    `)
    $('#list-movie').append(`
    <div class=" card-body p-1 m-1 bg-light d-flex align-items-center justify-content-center"  >
        <div class="card-body text-center text-white" style="background-color:black" >
            <font size="10">Movies joined</font>
        </div>
    </div>
        <div class="card-body bg-dark col-12 pt-3 pb-3 p-0 card-group d-flex flex-wrap " id="MovieJoined">
          </div>
    `)
    for(var movie of MovieCastReq.cast){
        $('#MovieJoined').append(`
        <div class="col-3 pt-3 pb-3 bg-dark">
        <a href="#" class="movieID" data-id="${movie.id}">
        <img src="https://image.tmdb.org/t/p/original${movie.poster_path}" class="card-img-top" alt="${movie.original_title}">
        <div class="card-body bg-white">
          <h5 class="card-title">${movie.original_title}</h5>
          </a>
          <p class="card-text">as ${movie.overview}</p>
        </div>
      </div>
        `)
        }
        window.scrollTo(0,0);
}

